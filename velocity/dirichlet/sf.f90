    MODULE LCP_2D

    !USE DERIVATIVES
    USE ACP
    USE VELOCITY
    !USE NAVIER_STOKES

    include 'accu.h'
    PRIVATE :: WP
    REAL(WP), PARAMETER, PRIVATE, DIMENSION(3,3) :: ID=RESHAPE((/1,0,0,0,1,0,0,0,1/),(/3,3/))


    integer, PARAMETER :: ny = 128
    INTEGER, PARAMETER :: NV = LP, N = NV *NY, N3=NY*3
    real(wp), parametER :: hy = 1.d0/dble(ny)

    REAL(WP), DIMENSION(NV) :: UBV
    REAL(WP), DIMENSION( 1:NY,NV) :: U3, FU3
    REAL(WP), DIMENSION(1:NY, 3) :: V3

    REAL(WP), PRIVATE, DIMENSION(3,3 ,1:NY), SAVE :: TAUV
    REAL(WP), DIMENSION(1:NY,3,3 ), SAVE :: DVDS


    REAL(WP), PRIVATE, DIMENSION(0:NY+1) :: FVB

    REAL(WP), PRIVATE, DIMENSION(3,3) :: SM, DV
    REAL(WP), DIMENSION( 1:NY,NV) :: D1X, D2X, D1Y, D2Y, D2U, DAGP,UAY,FTUY
    REAL(WP), PRIVATE, DIMENSION(3) :: M1
    REAL(WP), PRIVATE, DIMENSION(10) :: M3
    REAL(WP), PRIVATE, DIMENSION(15) :: FM

    REAL(WP), DIMENSION(0:NY) :: YS

    ! ACP parameter

    REAL(WP), PRIVATE :: PN,PAL,PN1,PDs,PACT,PG,PAL0,Pre2,Pre3
    REAL(WP), PRIVATE ::  eta
    SAVE PN,ER,PD, eta,pg,pre2,pre3,pal0
    
    REAL(WP) :: FOURpiN1, FOURpiO3Al, FOURpiO5N,PI

    INTEGER, PRIVATE, SAVE :: ORDER

    !  SDC method

    INTEGER :: F_PARA

    CONTAINS

    SUBROUTINE SETANPE(PNN, PPAL, PNN1, PPDS, PCT, AL, et, p2, p3, gg)
    IMPLICIT NONE
    REAL(WP) :: PNN, PPAL, PNN1, PPDS, PCT,AL,et,p2,p3,gg

    PN = PNN
    PAL = PPAL
    PN1 = PNN1
    PDs = PPDS
    PACT = PCT
    PAL0 = AL*GG/6.D0
    eta = 2./et
    Pre2 = 1./p2
    Pre3 = 1./p3
    PG = gg
    PI=4.D0*DATAN(1.D0)
    
    FOURpiN1   =  4*PI*PN1 - 2*PI*PN
    FOURpiO5N =  4*PI/5. * PN
    FOURpiO3Al =  4*PI/3. * PAl

    END SUBROUTINE SETANPE

    !**********************************************************************!


    SUBROUTINE SMOLUCHOWSKI(U, T, F)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN)  :: U
    REAL(WP), DIMENSION(:), INTENT(OUT) :: F
    REAL(WP) :: T

    INTEGER :: OS, KY

    ! RESHAPE

    OS = 0
    DO KY = 1, NY

        U3( KY,:) = U(OS+1:OS+NV)
        OS = OS+NV
    END DO

    ! Calculate the right hand side of the Smoluchowski equation
    ! with Maier-Saupe potential:

    CALL FVALUE_1(U3,V3,FU3)

    ! RESHAPE the result

    OS = 0
    DO KY = 1, NY

        F(OS+1:OS+NV) = FU3(KY,:)
        OS = OS + NV

    END DO

    END SUBROUTINE SMOLUCHOWSKI


    ! *********************************************************************** !


    ! *********************************************************************** !


    SUBROUTINE FVALUE_1(U, V, F)
    IMPLICIT NONE

    ! U( 1:NY,NV), F the same
    ! V( 1:NY, 3)

    REAL(WP), DIMENSION(:,:) :: U, V, F
    REAL(WP), DIMENSION(NV) :: AI, D1AIX, D2AIX, D1AIY, D2AIY, FV,PDF
    REAL(WP) ::   S1Y, S2Y

    INTEGER :: K, KY

    S1Y = 1.D0/(2.D0*HY)
    S2Y = 1.D0/(HY*HY)

    !  Find Du, D^2u

    DO K = 1, NV

        FVB(1:NY) = U( :,K)
        !FVB(0) = FVB(NY)
        !FVB(NY+1) = FVB(1)
        FVB(0)=UBV(K)
        FVB(NY)=UBV(K)
        !FVB(NY+1) = 2*FVB(NY) - FVB(NY-1)
        FVB(NY+1) = 3*FVB(NY) - 3*FVB(NY-1) + FVB(NY-2)
        
        D1Y( 1:NY,K) = S1Y*(FVB(2:NY+1)-FVB(0:NY-1))
        D2Y( 1:NY,K) = S2Y*(FVB(2:NY+1)-2.D0*FVB(1:NY)+FVB(0:NY-1))

    END DO
    !d1x,d2x added on Jan 12th
    D1X=0
    D2X=0



    !***  loop through grid points

    F = 0.D0
    DO KY = 1, NY

        AI = U( KY,:)
        D1AIX = D1X( KY,:)
        D2AIX = D2X( KY,:)
        D1AIY = D1Y( KY,:)
        D2AIY = D2Y( KY,:)

        DV = DVDS( KY,:,:)

        CALL DFDT(AI, D1AIX, D2AIX, D1AIY, D2AIY, DV, FV)
        F( KY,1:NV) = FV

    END DO

    !return 
    
    S1Y = 2.*S1Y

    DO K = 1, 9

        FVB(1:NY) = U(:,K)
        FVB(0) = UBV(K)
        FVB(NY) = UBV(K)
        !FVB(NY+1) = 2*FVB(NY) -FVB(NY-1)
        FVB(NY+1) = 3*FVB(NY) - 3*FVB(NY-1) + FVB(NY-2)

        D1Y(1:NY,K) = S1Y*(FVB(1:NY)-FVB(0:NY-1))
        UAY(1:NY,K) = 0.5*(FVB(1:NY)+FVB(0:NY-1))

    END DO

    ! Find averages of ther moments

    DO K = 10, NV

        FVB(1:NY) = U(:,K)
        FVB(0) = UBV(K)
        FVB(NY) = UBV(K)
        !FVB(NY+1) = 2*FVB(NY) -FVB(NY-1)
        FVB(NY+1) = 3*FVB(NY) - 3*FVB(NY-1) + FVB(NY-2)

        UAY(1:NY,K) = 0.5*(FVB(1:NY)+FVB(0:NY-1))

    END DO


    ! calculate F.DU/DX and F.DU/DY

    D1AIY = 0

    DO KY = 1, NY

        AI = UAY(KY,:)
        D1AIY(1) = FOURpiN1 * D1Y(KY,1)
        D1AIY(2:4) = -FOURpiO3Al * D1Y(KY,2:4)
        D1AIY(5:9) = -FOURpiO5N * D1Y(KY,5:9)
        CALL FTGO(2, AI, D1AIY, PDF)
        FTUY(KY,1:NV) = PDF

    END DO

    ! Find D(F.DU/DX)/DX and D(F.DU/DY)/DY

    DO K = 1, NV

        FVB(1:NY) = FTUY(:,K)


        D1Y(1:NY-1,K) = S1Y*(FVB(2:NY)-FVB(1:NY-1))
        D1Y(NY,K) = 2.*D1Y(NY-1,K) - D1Y(NY-2,K)   
    END DO
    
 
    Do K = 1, NV
        F(:,K) = F(:,K) + PDs * D1Y(:,K)
    END DO

    !	print *, maxval(abs(f)), maxval(abs(ai))

    ! Add in the convection term

    !Do K = 1, NV
    !    F(:,K) = F(:,K) - V(:,1)*D1X(:,K) - V(:,2)*D1Y(:,K)
    !END DO



    END SUBROUTINE FVALUE_1


    !***************************************************************************!

    SUBROUTINE FV_11(U, DV, DA2P, F)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN) :: U
    REAL(WP), DIMENSION(:,:), INTENT(IN) :: DV, DA2P
    REAL(WP), DIMENSION(:), INTENT(OUT) :: F

    F = 0
    !	CALL FV(U,DA2P,PN,PA,DV(1,2),F)


    END SUBROUTINE FV_11

    !********************************************************************


    !********************************************************************

    SUBROUTINE DTAUM(U,TAU,TIME)
    IMPLICIT NONE
    DOUBLE PRECISION, DIMENSION( 1:NY,NV) :: U
    DOUBLE PRECISION, DIMENSION(:,:,:) :: TAU

    INTEGER ::    K
    REAL(WP) :: TIME

    REAL(WP) ::  S1Y, S2X, S2Y


    S1Y = 1.D0/(2.D0*HY)
    S2Y = 1.D0/(HY*HY)

    !------------ Compute < Grad \mu >, and its divergence  ------------------!

    !  Find D(pdf), first 9 modes only

    DO K = 1, 9


        FVB(1:NY) = U(:,K)
        !FVB(0) = FVB(NY)
        !FVB(NY+1) = FVB(1)
        FVB(0)=UBV(K)
        FVB(NY)=UBV(K)


        D1Y( 1:NY,K) = S1Y*(FVB(2:NY+1)-FVB(0:NY-1))



    END DO

    !  4.19 = 4*Pi/3

    !  2.5133 = 3/2*8*pi/15

    !  This is the average of the potential gradient over the unit sphere:

    FV3A(:,1) = 6.28*(2*PN1-PN) * U(:,1)*D1X(:,1) &
        - PAL* 4.19 * (2*(U(:,4)*D1X(:,4) + U(:,2)*D1X(:,2)) + U(:,3)*D1X(:,3)) &
        - PN* 2.5133* ( U(:,7)*D1X(:,7) + 2*(U(:,8)*D1X(:,8) + U(:,5)*D1X(:,5) +    &
        U(:,9)*D1X(:,9) + U(:,6)*D1X(:,6)) )

    FV3A(:,2) = 6.28*(2*PN1-PN) * U(:,1)*D1Y(:,1) &
        - PAL* 4.19 * (2*(U(:,4)*D1Y(:,4) + U(:,2)*D1Y(:,2)) + U(:,3)*D1Y(:,3)) &
        - PN* 2.5133* ( U(:,7)*D1Y(:,7) + 2*(U(:,8)*D1Y(:,8) + U(:,5)*D1Y(:,5) +    &
        U(:,9)*D1Y(:,9) + U(:,6)*D1Y(:,6)) )


    FVB(1:NY) = FV3A( :,2)
    !FVB(0) = FVB(NY)
    !FVB(NY+1) = FVB(1)
    FVB(0)=UBV(K)
    FVB(NY)=UBV(K)

    D1Y( 1:NY,1) = S1Y*(FVB(2:NY+1)-FVB(0:NY-1))



    !  This is the divegence of the (average of the gradident of potentail):

    !DAGP = D1X(:,:) + D1Y(:,:)
    DAGP=0
    FV3A=0
    !---------------------------------


    ! Cauculate the elastic stress

    CALL TAUM_VALUE(U,TAU,TIME)
    
    DVDS = 0.

    DO K = 1, 3




        FVB(1:NY) = TAU(K,2, :)
        !FVB(   0) = FVB(2)
        FVB(0) = 3*FVB(1) - 3*FVB(2) + FVB(3)
        FVB(NY+1) = FVB(NY-1)
        !FVB(0)=UBV(K)
        !FVB(NY)=UBV(K)
        !FVB(NY+1)=2*FVB(NY)-FVB(NY-1)
        DVDS( 1:NY,K,2) = S1Y*(FVB(2:NY+1)-FVB(0:NY-1))   ! dTAU(*,2)/dy


    END DO

    !  Div(Tau_e) - Div(<Grad U>)

    FV3A(:,1) = DVDS(:,1,1) + DVDS(:,1,2) - FV3A(:,1)
    FV3A(:,2) = DVDS(:,2,1) + DVDS(:,2,2) - FV3A(:,2)
    FV3A(:,3) = DVDS(:,3,1) + DVDS(:,3,2)



    END SUBROUTINE DTAUM


    !*****************************************************************************!


    !*****************************************************************************!

    SUBROUTINE TAUM_VALUE(U,TAU,TIME)
    IMPLICIT NONE
    REAL(WP), DIMENSION( 1:NY,NV) :: U
    REAL(WP), DIMENSION(:,1:,1:) :: TAU
    REAL(WP), DIMENSION(NV) :: PDF
    REAL(WP), DIMENSION(3,3) :: DV


    INTEGER ::   KY
    REAL(WP) :: TIME

    TAU = 0.D0


    DO KY = 1, NY

        PDF = U( KY,:)
        CALL MOMENTS(PDF)
        DV = DVDS( KY,:,:)
        CALL SINGLE_TAUM(PDF(1), M1, SM,FM,DV,TAU(:,:, KY))

    END DO


    END SUBROUTINE TAUM_VALUE


    !*****************************************************************************!

    SUBROUTINE SINGLE_TAUM(A00, TM, SM, FM, VD, TAU)
    IMPLICIT NONE
    REAL(WP) :: A00, C1
    REAL(WP), DIMENSION(:) :: TM
    REAL(WP), DIMENSION(:,:) :: SM, TAU
    REAL(WP), DIMENSION(3,3) :: VD,DV, X1, X2, X3, X4, X5
    REAL(WP), DIMENSION(:) :: FM
    INTEGER :: I, J
    
    DV=0.5*(VD+TRANSPOSE(VD))
    !  SM(3,3), D(3,3), TAU(3,3)

    !M:M4
    X3(1,1) = (SM(1,1)*FM(1)+SM(2,2)*FM(3)+SM(3,3)*FM(10) &
        +2._WP*(SM(1,2)*FM(2)+SM(1,3)*FM(6)+SM(2,3)*FM(7)))

    X3(1,2) = (SM(1,1)*FM(2)+SM(2,2)*FM(4)+SM(3,3)*FM(11) &
        +2._WP*(SM(1,2)*FM(3)+SM(1,3)*FM(7)+SM(2,3)*FM(8)))

    X3(1,3) = (SM(1,1)*FM(6)+SM(2,2)*FM(8)+SM(3,3)*FM(13) &
        +2._WP*(SM(1,2)*FM(7)+SM(1,3)*FM(10)+SM(2,3)*FM(11)))

    X3(2,2) = (SM(1,1)*FM(3)+SM(2,2)*FM(5)+SM(3,3)*FM(12) &
        +2._WP*(SM(1,2)*FM(4)+SM(1,3)*FM(8)+SM(2,3)*FM(9)))

    X3(2,3) = (SM(1,1)*FM(7)+SM(2,2)*FM(9)+SM(3,3)*FM(14) &
        +2._WP*(SM(1,2)*FM(8)+SM(1,3)*FM(8)+SM(2,3)*FM(12)))

    X3(3,3) = (SM(1,1)*FM(10)+SM(2,2)*FM(12)+SM(3,3)*FM(15) &
        +2._WP*(SM(1,2)*FM(11)+SM(1,3)*FM(13)+SM(2,3)*FM(14)))

    X3(2,1) = X3(1,2)
    X3(3,1) = X3(1,3)
    X3(3,2) = X3(2,3)

    !M.M
    X5 = MATMUL(SM, SM)

    C1 = Sqrt(4.D0*3.1415926535897932D0)*A00

    TAU = (1.D0+PACT)*(SM-C1*ID/3.D0)-PN*(X5-X3)

    X1(1,1) = M3(1)*M1(1)+M3(2)*M1(2)+M3(3)*M1(3)
    X1(1,2) = M3(2)*M1(1)+M3(4)*M1(2)+M3(5)*M1(3)
    X1(1,3) = M3(3)*M1(1)+M3(5)*M1(2)+M3(6)*M1(3)
    X1(2,2) = M3(4)*M1(1)+M3(7)*M1(2)+M3(8)*M1(3)
    X1(2,3) = M3(5)*M1(1)+M3(8)*M1(2)+M3(9)*M1(3)
    X1(3,3) = M3(6)*M1(1)+M3(9)*M1(2)+M3(10)*M1(3)

    X1(2,1) = X1(1,2)
    X1(3,1) = X1(1,3)
    X1(3,2) = X1(2,3)

    DO I = 1, 3
        DO J = 1, 3
            X2(I,J) = M1(I)*M1(J)
        END DO
    END DO

    !  PAL0 = ALPHA/6*2

    TAU = PG*(TAU-PAL0*(X2-X1))

    X4 = MATMUL(DV, SM) + MATMUL(SM, DV)

    !D:M4
    X3(1,1) = (DV(1,1)*FM(1)+DV(2,2)*FM(3)+DV(3,3)*FM(10) &
        +2._WP*(DV(1,2)*FM(2)+DV(1,3)*FM(6)+DV(2,3)*FM(7)))

    X3(1,2) = (DV(1,1)*FM(2)+DV(2,2)*FM(4)+DV(3,3)*FM(11) &
        +2._WP*(DV(1,2)*FM(3)+DV(1,3)*FM(7)+DV(2,3)*FM(8)))

    X3(1,3) = (DV(1,1)*FM(6)+DV(2,2)*FM(8)+DV(3,3)*FM(13) &
        +2._WP*(DV(1,2)*FM(7)+DV(1,3)*FM(10)+DV(2,3)*FM(11)))

    X3(2,2) = (DV(1,1)*FM(3)+DV(2,2)*FM(5)+DV(3,3)*FM(12) &
        +2._WP*(DV(1,2)*FM(4)+DV(1,3)*FM(8)+DV(2,3)*FM(9)))

    X3(2,3) = (DV(1,1)*FM(7)+DV(2,2)*FM(9)+DV(3,3)*FM(14) &
        +2._WP*(DV(1,2)*FM(8)+DV(1,3)*FM(8)+DV(2,3)*FM(12)))

    X3(3,3) = (DV(1,1)*FM(10)+DV(2,2)*FM(12)+DV(3,3)*FM(15) &
        +2._WP*(DV(1,2)*FM(11)+DV(1,3)*FM(13)+DV(2,3)*FM(14)))

    X3(2,1) = X3(1,2)
    X3(3,1) = X3(1,3)
    X3(3,2) = X3(2,3)

    TAU = TAU +Pre2*X4 + Pre3*X3


    ! Suppose theta = 0


    END SUBROUTINE SINGLE_TAUM

    !*********************************************************************!


    SUBROUTINE SINGLE_TAUV(DD, TAU)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:,:) :: DD, TAU

    !     REAL(WP), DIMENSION(:,:) :: SMD, DD, TAU
    !     REAL(WP), DIMENSION(:) :: FMD

    !  SMD(3,3), DD(3,3), TAU(3,3), FMD(12)

    TAU = eta*DD

    END SUBROUTINE SINGLE_TAUV

    !*******************************************************************!


    !******************************************************************!


    SUBROUTINE MOMENTS(U)
    IMPLICIT NONE

    REAL(WP), DIMENSION(0:) :: U
    REAL(WP) :: SSQT1, SSQT2, SSQT0, SSQT3


    REAL(WP) :: SQT1, SQT2, SQT3, SQT4, SQT5, SQT6, Pi
    REAL(WP) :: DD1, DD2, DD3, DD4
    REAL(WP) :: a20,ra21,ra22,im21,im22,a40,ra41,ra42
    REAL(WP) :: ra43,ra44,im41,im42,im43,im44
    REAL(WP) :: a10,ra11,a30,ra31,ra32,ra33,im11,im31,im32,im33
    REAL(WP) :: b1,b2,b3,b4,b5,b6,b7,b8,b9
    REAL(WP) :: c1,c2,c3,c4,c5
    REAL(WP) :: MT1,MT2,MT3,MT4,MT5,MT6,MT7,MT8,MT9

    DATA   Pi/3.141592653589793238462_WP/
    DATA SQT1/0.798931687148128339089_WP/   ! 8/3Sqrt[Pi/35]
    DATA SQT2/0.301967794103315124826_WP/   ! 8/21Sqrt[Pi/5]
    DATA SQT3/0.427046949820794567566_WP/   ! 8/21Sqrt[2Pi/5]
    DATA SQT4/2.259720054749003313723_WP/   ! 16/3Sqrt[2Pi/35]
    DATA SQT5/2.588834550074265672053_WP/   ! 2Sqrt[8Pi/15]
    DATA SQT6/1.294417275037132836026_WP/   ! 2Sqrt[2Pi/15]
    DATA  DD1/2.449489742783178098197_WP/   ! 3Sqrt[2/3]
    DATA  DD2/5.196152422706631880582_WP/   ! 3Sqrt[3]
    DATA  DD3/0.270088205852269108921_WP/   ! 8/105 2Sqrt[Pi]
    DATA  DD4/1.207871176413260499305_WP/   ! 8/105 20Sqrt[Pi/5]

    DATA SSQT3/2.046653415892977_WP/       ! Sqrt[4Pi/3]
    DATA SSQT0/1.4472025091165353_WP/      ! Sqrt[2Pi/3]
    DATA SSQT1/-0.52844363968080146845_WP/ ! -2/3 Sqrt[Pi/5]
    DATA SSQT2/1.2944172750371328360_WP/   ! Sqrt[8Pi/15]

    DATA MT1/1.7366430109398425_WP/     ! 6/5 Sqrt[2Pi/3]
    DATA MT2/0.4641373678623143_WP/     ! 6/5 Sqrt[Pi/21]
    DATA MT3/0.5991987653610963_WP/     ! 2 Sqrt[Pi/35]
    DATA MT4/0.5788810036466141_WP/     ! 2/5 Sqrt[2Pi/3]
    DATA MT5/0.15471245595410477_WP/    ! 2/5 Sqrt[Pi/21]
    DATA MT6/0.40933068317859544_WP/    ! 2/5 Sqrt[Pi/3]
    DATA MT7/0.2679698342762715_WP/     ! 2/5 Sqrt[Pi/7]
    DATA MT8/0.4892437432134499_WP/     ! 2 Sqrt[2Pi/105]
    DATA MT9/0.6188498238164191_WP/     ! 8/5 Sqrt[Pi/21]


    M1(1) =  -2.D0*SSQT0 *U(3)

    M1(2) =   2.D0*SSQT0 *U(1)

    M1(3) =   SSQT3 *U(2)

    c1 = Sqrt(4.D0*3.1415926535897932D0)*U(0)

    SM(1,1) =  SSQT1*U(6)+SSQT2*U(8) + c1/3._WP
    SM(2,2) =  SSQT1*U(6)-SSQT2*U(8) + c1/3._WP
    SM(1,2) = -SSQT2*U(4)
    SM(1,3) = -SSQT2*U(7)
    SM(2,3) =  SSQT2*U(5)
    SM(2,1) =  SM(1,2)
    SM(3,1) =  SM(1,3)
    SM(3,2) =  SM(2,3)
    SM(3,3) =  c1 - SM(1,1) - SM(2,2)



    a10 = U(2)
    ra11 = U(3)
    im11 = U(1)


    a30 = U(12)
    ra31 = U(13)
    ra32 = U(14)
    ra33 = U(15)
    im31 = U(11)
    im32 = U(10)
    im33 = U(9)


    M3(1) = -MT1*ra11 + MT2*ra31 - MT3*ra33
    M3(2) =  MT4*im11 - MT5*im31 + MT3*im33
    M3(3) =  MT6*a10  - MT7*a30  + MT8*ra32
    M3(4) =  MT4*ra11 + MT5*ra31 + MT3*ra33
    M3(5) = -MT8*im32
    M3(6) = -MT9*ra31 - MT4*ra11
    M3(7) =  MT1*im11 - MT2*im31 - MT3*im33
    M3(8) =  MT6*a10  - MT7*a30  - MT8*ra32
    M3(9) =  MT9*im31 + MT4*im11
    M3(10) = 3.D0*MT6*a10 + 2.D0*MT7*a30


    a20 = U(6)    !A(1)
    ra21 = U(7)    !A(2)
    ra22 = U(8)    !A(3)
    im21 = U(5)    !A(LMT+1)
    im22 = U(4)    !A(LMT+2)


    a40 = U(20)
    ra41 = U(21)
    ra42 = U(22)
    ra43 = U(23)
    ra44 = U(24)
    im41 = U(19)
    im42 = U(18)
    im43 = U(17)
    im44 = U(16)

    b1 = -SQT1*ra43;
    b2 =  SQT1*im43;
    b3 =  SQT2*(ra41-DD1*ra21);
    b4 = -SQT2*(im41-DD1*im21);
    b5 = -SQT3*(ra42-DD2*ra22);
    b6 =  SQT3*(im42-DD2*im22);
    b7 =  SQT4*ra44;
    b8 = -SQT4*im44;
    b9 =  DD3*a40-DD4*a20+8._WP/15._WP;

    c1 =  2._WP/3._WP*(1._WP-Sqrt(4._WP*Pi/5._WP)*a20)
    c2 =  SQT5*ra22
    c3 = -SQT5*im22
    c4 = -SQT6*ra21
    c5 =  SQT6*im21

    FM(1) = 3._WP*b9/8._WP + b5/2._WP +b7/8._WP          ! a1111
    FM(2) = b6/4._WP + b8/8._WP                  ! a1112
    FM(3) = b9/8._WP - b7/8._WP                  ! a1122
    FM(4) = b6/4._WP - b8/8._WP                  ! a1222
    FM(5) = 3._WP*b9/8._WP -b5/2._WP +b7/8._WP           ! a2222

    FM(6) = 3._WP*b3/4._WP + b1/4._WP                ! a1113
    FM(7) = 0.25_WP*(b2 + b4)               ! a1123
    FM(8) = 0.25_WP*(-b1 + b3)              ! a1223
    FM(9) = 0.25_WP*(3._WP*b4 - b2)             ! a2223

    FM(10) = 0.5_WP*(c1 + c2 - b9 - b5)     ! a1133
    FM(11) = 0.5_WP*(c3 -b6)                ! a1233
    FM(12) = 0.5_WP*(c1 - c2 - b9 + b5)     ! a2233

    FM(13) = c4 - b3                     ! a1333
    FM(14) = c5 - b4                     ! a2333

    FM(15) = 1._WP - 2._WP* c1 + b9              ! a3333

    END SUBROUTINE MOMENTS


    !******************************************************************!


    !******************************************************************!




    END MODULE LCP_2D
