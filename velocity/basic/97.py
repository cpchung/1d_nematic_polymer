#%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
import math
import sys
import inspect
# read file first
file="out.97"
with open(file, "r") as ins:
    array = []
    for line in ins:
        temp= [float(x) for x in line.split()]
        array.append(temp)
        
# print array[0][1]
# print 'number of lines in '+file+' is: '+str(len(array))

# the unmodified grid
numGrid=0
while array[numGrid][0] == array[numGrid+1][0]:
    numGrid=numGrid+1
numGrid+=1    
# print 'number of Grids: '+str(numGrid)

numTimeStep=len(array)/numGrid
# print 'number of tStep: '+str(numTimeStep)


def pltVelocityOrientation():
	# X is actually y, Y is t
	X,Y = np.meshgrid(np.arange(0, len(array)/numGrid,1 ), np.arange(0,numGrid,1))

	U= [-1*row[4] for row in array]
	V= [row[2] for row in array]

	#**********fix the plot****************
	tempU = [0] * len(U)
	tempV = [0] * len(V)
	it=0
	for t in range(0,numTimeStep):
		counter=t
		for n in range(0,numGrid):
			# print it,counter
			tempU[counter]=U[it]
			tempV[counter]=V[it]
			it=it+1
			counter=counter+numTimeStep
	U=tempU
	V=tempV

	plt.figure()
	Q = plt.quiver(X,Y,U,V,pivot='mid', color='r', units='width')
	l, r, b, t = plt.axis()
	dx, dy = r - l, t - b
	plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
	plt.xlabel('Time')
	plt.ylabel('Y')
	plt.title(inspect.stack()[0][3])
	# save to eps and png

	filename='.'+inspect.stack()[0][3]
	plt.legend(loc='best')	
	# plt.savefig('./t'+str(len(array)/numGrid)+filename+'.eps', format='eps', dpi=1000)
	plt.savefig('./t'+str(len(array)/numGrid)+filename+'.png')
	# print "saved as "+'./t'+str(len(array)/numGrid)+filename

def pltVelocityAngle():
	U = [-1*row[4] for row in array]
	V = [row[2] for row in array]
	
	angle=[]
	U=U[-numGrid:]
	V=V[-numGrid:]

	for a in range(0,len(U)):
		if U[a]!=0:
			angle.append(V[a]/U[a])
		else:
			angle.append(V[a]/sys.float_info.min)

	angle=np.arctan(angle)
	for j in range(len(angle)):
		if angle[j]<0:
			angle[j]=-angle[j]
	angle=[math.degrees(x) for x in angle]

	plt.figure()
	plt.plot(np.linspace(0,1,len(angle)),angle)

	l, r, b, t = plt.axis()
	dx, dy = r - l, t - b

	plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
	plt.xlabel('grid in last time step')
	plt.ylabel('velocity angle in the last time step')
	# plt.title(inspect.stack()[0][3], loc='right')
	
	with open("fort.95", "r") as ins:
	    parameters = []
	    for line in ins:
	    	lineTrimmed=line.rstrip('\n').rstrip('\r')
	        parameters.append(lineTrimmed)

	plt.title(' PN='+parameters[1].split()[0]
		+', PAL='+parameters[1].split()[1]
		+', PDS='+parameters[2].split()[1]
		+', PCT='+parameters[2].split()[2]
		+', PV='+parameters[2].split()[3],  fontsize=16,loc='left')

	# save to eps and png
	filename='.'+inspect.stack()[0][3]
	plt.legend(loc='best')			
	# plt.savefig('./t'+str(len(array)/numGrid)+filename+'.eps', format='eps', dpi=1000)
	plt.savefig('./t'+str(len(array)/numGrid)+filename+'.png')
	# print "saved as "+'./t'+str(len(array)/numGrid)+filename


def pltVelocityOverGrid():
	# read file first
	V = [row[6] for row in array]
	
	plt.figure()
	for j in range(numTimeStep):
		plt.plot(np.linspace(0,1,numGrid),V[numGrid*j:numGrid*(j+1)],label='{time}'.format(time=j))

	l, r, b, t = plt.axis()
	dx, dy = r - l, t - b

	plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
	plt.xlabel('Grid')
	plt.ylabel('Velocity')
	plt.title(inspect.stack()[0][3])

	# save to eps and png
	filename='.'+inspect.stack()[0][3]
	plt.legend(loc='best')			
	# plt.savefig('./t'+str(len(array)/numGrid)+filename+'.eps', format='eps', dpi=1000)
	plt.savefig('./t'+str(len(array)/numGrid)+filename+'.png')
	# print "saved as "+'./t'+str(len(array)/numGrid)+filename

if __name__ == '__main__':
	# print(sys.argv, len(sys.argv))

	if len(sys.argv)-1 ==0:
		pltVelocityOverGrid()
		pltVelocityAngle()
		pltVelocityOrientation()
		# plt.show()
		
	else:
		print "input wrong in "+sys.argv[0]


