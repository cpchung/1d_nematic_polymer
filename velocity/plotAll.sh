rm log.plot
size=3000
for (( i=0; i <= $size; i++ )) do
	echo $i >> log.plot
	if [ -d "$i.folder" ]; then
		cp ./basic/*.py $i.folder
		cp ./basic/*.sh $i.folder
		cd $i.folder
		bash sinPlot.sh 2>> stderr
		#mv *.png ../temp
		# mv fort.95 ../temp
		# mv fort.96 ../temp
		# mv info.txt ../temp
		#rm ./*
		#cp ../temp/* .
		#rm ../temp/*
		cd ..
  	else
  		git add --all .
  		break
	fi
done
echo 'done' >> log.plot
