    MODULE VELOCITY

    include 'accu.h'
    PRIVATE :: WP

    INTEGER, PRIVATE, SAVE :: PRINTC
    DATA PRINTC/0/

    INTEGER, PRIVATE, PARAMETER :: NY = 128
    INTEGER, PRIVATE, PARAMETER :: N3 = 3*NY
    REAL, PRIVATE, PARAMETER :: FA = 0.D0, FB = 1.D0, FC = 0.D0, FD = 1.D0
    REAL(WP), PRIVATE, PARAMETER :: HY = (FD-FC)/NY

    REAL(WP), PRIVATE, DIMENSION(0:NY+1) :: FVB

    REAL(WP), PRIVATE, DIMENSION(1:NY), SAVE :: DAGP
    REAL(WP), DIMENSION(1:NY,3), SAVE :: FV3, FV3A
    REAL(WP), PRIVATE, DIMENSION(1:NY,3), SAVE :: V3

    ! LCP parameter

    REAL(WP), PRIVATE, SAVE :: PM3,PV

    INTEGER, PRIVATE, SAVE :: ORDER

    CONTAINS


    SUBROUTINE SET_NS_VALUE(P3,PPV)
    IMPLICIT NONE

    REAL(WP) :: P3,PPV

    PM3 = 1./P3
    PV=PPV

    FVB = 0.

    FV3 = 0.
    FV3A = 0.

    END SUBROUTINE SET_NS_VALUE

    !**********************************************************************!

    !**********************************************************************!

    SUBROUTINE MOMENTUM(U, T, F)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN)  :: U
    REAL(WP), DIMENSION(:), INTENT(OUT) :: F
    REAL(WP) :: T, S1Y, S2Y
    REAL(WP), DIMENSION(1:NY,3,3), SAVE :: DVDS

    INTEGER :: OS,KY,K

    S1Y = 1.D0/(2.D0*HY)
    S2Y = 1.D0/(HY*HY)

    !  V3(1:NX,1:NY,3) = RESHAPE(U,(/NX,NY,3/))

    OS = 0
    DO KY = 1, NY
        V3(KY,:) = U(OS+1:OS+3)
        OS = OS + 3
    END DO

    FV3 = 0.
    DVDS = 0.



    !***  Compute viscous stress (Laplace(V))  ***

    DVDS = 0.
    DO K  = 1, 1  ! only x-components of the flow field

        FVB(1:NY) = V3(:,K)
        FVB(   0) = 0
        FVB(  NY) = PV
        !FVB(NY+1) = 2*FVB(NY)-FVB(NY-1)
        FVB(NY+1) = 3*FVB(NY) - 3*FVB(NY-1) + FVB(NY-2)
        DVDS(:,K,2) = S2Y*(FVB(2:NY+1)-2.D0*FVB(1:NY)+FVB(0:NY-1))

    END DO  ! 3 components of the flow field


    !   -(V.Grad)V + mu3*Delta(V)

    FV3(:,1) = FV3(:,1) + PM3*(DVDS(:,1,1)+DVDS(:,1,2))
    FV3(:,2) = FV3(:,2) + PM3*(DVDS(:,2,1)+DVDS(:,2,2))
    FV3(:,3) = FV3(:,3) + PM3*(DVDS(:,3,1)+DVDS(:,3,2))

    !   -(V.Grad)V + mu3*Delta(V) -Grad(p)+Div(Tau_e)

    !!  FV3A = -Grad(p)+Div(Tau_e), computed before the Velocity solver.

    FV3 = FV3 + FV3A

    !  F = RESHAPE(FV3(-NY1:NY1,1:NZ,:), (/N3/))

    OS = 0
    DO KY = 1, NY
        F(OS+1:OS+3) = FV3(KY,:)
        OS = OS + 3
    END DO

    END SUBROUTINE MOMENTUM


    !**********************************************************************!


    !**********************************************************************!

    SUBROUTINE MOMENTUM_O(U, T, F)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN)  :: U
    REAL(WP), DIMENSION(:), INTENT(OUT) :: F
    REAL(WP) :: T, S1Y,  S2Y
    REAL(WP), DIMENSION(1:NY,3,3), SAVE :: DVDS

    INTEGER :: OS,KY,K

    S1Y = 1.D0/(2.D0*HY)
    S2Y = 1.D0/(HY*HY)

    !  V3(1:NX,1:NY,3) = RESHAPE(U,(/NX,NY,3/))

    OS = 0
    DO KY = 1, NY

        V3(KY,:) = U(OS+1:OS+3)
        OS = OS + 3
    END DO

    FV3 = 0.
    DVDS = 0.

    !!  DV/DS
    !
    !	DO K = 1, 1     !  3 components of the flow field
    !
    !              FVB(1:NY) = V3(KX,:,K)
    !              FVB(   0) = 0
    !              FVB(  NY) = PV
    !              FVB(NY+1) = 2*FVB(NY)-FVB(NY-1)
    !              DVDS(1:NY,K,2) = S1Y*(FVB(2:NY+1)-FVB(0:NY-1))
    !
    !        END DO   ! 3 components of the flow field
    !
    !!   -(V.Grad)V
    !
    !        FV3(:,1) = -V3(:,1)*DVDS(:,1,1) - V3(:,2)*DVDS(:,1,2)
    !        FV3(:,2) = -V3(:,1)*DVDS(:,2,1) - V3(:,2)*DVDS(:,2,2)
    !        FV3(:,3) = -V3(:,1)*DVDS(:,3,1) - V3(:,2)*DVDS(:,3,2)

    !***  Compute viscous stress (Laplace(V))  ***

    DO K  = 1, 1  ! 3 components of the flow field

        FVB(1:NY) = V3(:,K)
        FVB(   0) = 0
        FVB(  NY) = PV
        FVB(NY+1) = 2*FVB(NY)-FVB(NY-1)
        DVDS(:,K,2) = S2Y*(FVB(2:NY+1)-2.D0*FVB(1:NY)+FVB(0:NY-1))

    END DO  ! 3 components of the flow field


    !   -(V.Grad)V + mu3*Delta(V)

    FV3(:,1) = FV3(:,1) + PM3*(DVDS(:,1,1)+DVDS(:,1,2))
    FV3(:,2) = FV3(:,2) + PM3*(DVDS(:,2,1)+DVDS(:,2,2))
    FV3(:,3) = FV3(:,3) + PM3*(DVDS(:,3,1)+DVDS(:,3,2))

    !   -(V.Grad)V + mu3*Delta(V) -Grad(p)+Div(Tau_e)

    !!  FV3A = -Grad(p)+Div(Tau_e), computed before the Velocity solver.

    FV3 = FV3 + FV3A

    !  F = RESHAPE(FV3(-NY1:NY1,1:NZ,:), (/N3/))

    OS = 0
    DO KY = 1, NY
        F(OS+1:OS+3) = FV3(KY,:)
        OS = OS + 3
    END DO

    END SUBROUTINE MOMENTUM_O


    !**********************************************************************!


    !**********************************************************************!

    SUBROUTINE MOMENTUM_EX(U, T, F)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN)  :: U
    REAL(WP), DIMENSION(:), INTENT(OUT) :: F
    REAL(WP) :: T

    INTEGER :: OS,KY,KZ,K

    CALL MOMENTUM(U,T,F)

    END SUBROUTINE MOMENTUM_EX


    !**********************************************************************!


    !**********************************************************************!


    SUBROUTINE FLOW_FIELD(V, DVDS)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:,:)  :: V
    REAL(WP) :: S1Y
    REAL(WP), DIMENSION(:,:,:) :: DVDS

    INTEGER :: K,KY

    S1Y = 1.D0/(2.D0*HY)

    !***  first derivatives of velocity

    DVDS = 0.D0

    DO K  = 1, 1

        FVB(1:NY) = V(:,K)
        FVB(   0) = 0
        FVB(  NY) = PV
        !FVB(NY+1) = 2*FVB(NY)-FVB(NY-1)
        FVB(NY+1) = 3*FVB(NY) - 3*FVB(NY-1) + FVB(NY-2)
        DVDS(1:NY,K,2) = S1Y*(FVB(2:NY+1)-FVB(0:NY-1))

    END DO

    ENDSUBROUTINE FLOW_FIELD


    !***************************************************************************!


    !***************************************************************************!


    SUBROUTINE PRESSURE(V, P, TAU, TIME)
    IMPLICIT NONE
    REAL(WP), DIMENSION(1:NY,1:3) :: V
    REAL(WP), DIMENSION(3,3,1:NY) :: TAU
    REAL(WP), DIMENSION(1:NY) :: P

    REAL(WP) :: TIME

    P = 0.

    END SUBROUTINE PRESSURE



    !********************************************************************!


    !********************************************************************!

    SUBROUTINE GRADIENT_P(P)
    IMPLICIT NONE
    DOUBLE PRECISION, DIMENSION(1:NY) :: P

    !  FV3A = Div(Tau_e) - Grad(P)

    FV3A(:,1) = FV3A(:,1)
    FV3A(:,2) = FV3A(:,2)


    END SUBROUTINE GRADIENT_P


    !*****************************************************************************!


    END MODULE VELOCITY
