# %matplotlib inlineaaaaaa
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
# import math

# read file first
with open("tran.txt", "r") as ins:
    array = []
    for line in ins:
        temp= [float(x) for x in line.split()]
        array.append(temp)
        
# print array[0][1]
# column1= [row[1] for row in array]
# print len(column1)
print 'number of lines: '+str(len(array))

# the unmodified grid
numGrid=0
while array[numGrid][0] == array[numGrid+1][0]:
    numGrid=numGrid+1
numGrid+=1    
print 'number of Grids: '+str(numGrid)

numTimeStep=len(array)/numGrid
print 'number of tStep: '+str(numTimeStep)

# X is actually y, Y is t
X,Y = np.meshgrid(np.arange(0, len(array)/numGrid,1 ), np.arange(0,numGrid,1))
# X,Y = np.meshgrid(np.arange(np.arange(0,numGrid,1), np.arange(0, len(array)/numGrid,1 ))

# t = np.linspace(1,len(array)/64,len(array)/64)
# y = np.linspace(0,64-1,64)
# (X,Y) = np.meshgrid(t,y)

# U = [-1*row[4] for row in array]
U = [1*row[4] for row in array]
V = [row[2] for row in array]

#**********fix the plot****************
print U
print V

tempU = [0] * len(U)
tempV = [0] * len(V)
print tempU
print tempV

it=0
for t in range(0,numTimeStep):
	counter=t
	for y in range(0,numGrid):
		print it,counter
		tempU[counter]=U[it]
		tempV[counter]=V[it]
		it=it+1
		counter=counter+numTimeStep
print tempU
print tempV
U=tempU
V=tempV
#**********fix the plot****************

# normalize
# UV=zip(U, V)
# # print UV
# UV= [x/np.linalg.norm(x) for x in UV]
# # print UV
# U=[x[0]for x in UV]
# V=[x[1]for x in UV]
# print U[5]==UV[5][0]

# # testing
# U = np.cos(X)
# V = np.sin(Y)

# 1
plt.figure()
Q = plt.quiver(X, Y, U, V,
               pivot='mid', color='r', units='width')
# Q = plt.quiver(X, Y,U, V)
# qk = plt.quiverkey(Q, 0.5, 0.92, 2, r'$2 \frac{m}{s}$', labelpos='W',
#                    fontproperties={'weight': 'bold'})
l, r, b, t = plt.axis()
dx, dy = r - l, t - b

plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
plt.xlabel('Time')
plt.ylabel('Y')
plt.title('t,y plot')
# save to eps and png
plt.savefig('./t'+str(len(array)/numGrid)+'.eps', format='eps', dpi=1000)
plt.savefig('./t'+str(len(array)/numGrid)+'.png')
plt.show()